<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
             array(
                  'name' => 'Mickael',
                  'email' => 'mick@example.com',
                  'password' => Hash::make('secret'),
             )
        );

        User::create(
             array(
                  'name' => 'Romeo',
                  'email' => 'romeo@example.com',
                  'password' => Hash::make('secret'),
             )
        );
    }
}
