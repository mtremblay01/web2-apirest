<?php

use App\Station;
use Illuminate\Database\Seeder;

class StationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Station::create(
             array(
                  'description' => 'Macamic',
                  'lat' => '48.76',
                  'long' => '-79.06',
             )
        );

        Station::create(
            array(
                 'description' => 'La Sarre',
                 'lat' => '49.03',
                 'long' => '-79.56',
            )
       );
    }
}
