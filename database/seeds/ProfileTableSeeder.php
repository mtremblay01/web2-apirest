<?php

use Illuminate\Database\Seeder;
use App\Profile;

class ProfileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Profile::create(
             array(
                  'user_id' => 1,
                  'ddn' => '2018-12-12',
                  'web_site_url' => 'http://mickaeltremblay.com',
                  'facebook_url' => 'https://facebook.com',
                  'linkedin_url' => 'https://linkedin.com',
             )
        );
    }
}
