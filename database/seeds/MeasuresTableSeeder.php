<?php

use Illuminate\Database\Seeder;
use App\Measure;

class MeasuresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $measure = new Measure();
        $measure->station_id = 1;
        $measure->value = 15;
        $measure->description = "CO2";
        $measure->save();

        $measure = new Measure();
        $measure->station_id = 1;
        $measure->value = 25;
        $measure->description = "CO2";
        $measure->save();

        $measure = new Measure();
        $measure->station_id = 1;
        $measure->value = 10;
        $measure->description = "PH10";
        $measure->save();

        $measure = new Measure();
        $measure->station_id = 1;
        $measure->value = 13;
        $measure->description = "PH10";
        $measure->save();
    }
}
