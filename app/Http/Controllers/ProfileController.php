<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\ProfilePutRequest;
use App\Http\Requests\UserGetRequest;

class ProfileController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
     public function show(User $user)
     {
          if($user->profile != null)
               return $user->profile;
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */

    public function update(ProfilePutRequest $request, User $user)
    {
        $user->profile->ddn = $request->ddn;
        $user->profile->web_site_url = $request->web_site_url;
        $user->profile->facebook_url = $request->facebook_url;
        $user->profile->linkedin_url = $request->linkedin_url;
        $user->profile->save();

        return $user->profile;
    }
}
