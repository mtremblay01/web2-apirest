<?php

namespace App\Http\Controllers;

use App\Station;
use App\Http\Requests\StationRequest;
use App\Http\Resources\Station as StationResource;
use Illuminate\Http\Request;

class StationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $stations = Station::paginate(5);
        return StationResource::collection($stations);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StationRequest $request)
    {
        $station = new Station($request->all());
        $station->save();

        return  $station;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Station  $station
     * @return \Illuminate\Http\Response
     */
    public function show(Station $station)
    {
         return new StationResource($station);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Station  $station
     * @return \Illuminate\Http\Response
     */
    public function update(StationRequest $request, Station $station)
    {
          $station = Station::findOrFail($station->id);
          $station->description = $request->input('description');
          $station->lat = $request->input('lat');
          $station->long = $request->input('long');
          $station->save();
          return new StationResource($station);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Station  $station
     * @return \Illuminate\Http\Response
     */
    public function destroy(Station $station)
    {
        $station->delete();

        return $station;
    }
}
