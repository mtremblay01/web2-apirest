<?php

namespace App\Http\Controllers;

use App\Measure;
use Illuminate\Http\Request;
use App\Http\Resources\MeasureResource;
use App\Http\Requests\MeasurePostRequest;

class MeasureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return MeasureResource::collection(Measure::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MeasurePostRequest $request)
    {
        $measure = new Measure();
        $measure->station_id = $request->station_id;
        $measure->value = $request->value;
        $measure->description = $request->description;
        $measure->save();

        return $measure;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Measure  $measure
     * @return \Illuminate\Http\Response
     */
    public function show(Measure $measure)
    {
        return new MeasureResource($measure);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Measure  $measure
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MeasurePostRequest $measure)
    {
        $measure->station_id = $request->station_id;
        $measure->value = $request->value;
        $measure->description = $request->description;

        return $measure;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Measure  $measure
     * @return \Illuminate\Http\Response
     */
    public function destroy(Measure $measure)
    {
        $measure->delete();

        return $measure;
    }
}
