<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Station extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
             'id' =>$this->id,
             'description' => $this->description,
             'lat' => $this->lat,
             'long' => $this->long,
             'measure' => $this->measure,
        ];
    }
}
