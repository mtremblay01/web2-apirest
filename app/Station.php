<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Station extends Model
{
     protected $with = [
          'Measure'
     ];

     protected $fillable = [
         'description',
         'lat',
         'long'
     ];

    public function measure(){
         return $this->hasMany('App\Measure');
    }
}
