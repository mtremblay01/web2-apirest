<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProfileControllerTest extends TestCase
{
     use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */

     public function testGetProfile(){
         $response = $this->get('/api/users/1/profile');

         $response->assertJsonFragment(['ddn' => '2018-12-12',
                                       'web_site_url' => 'http://mickaeltremblay.com',
                                       'facebook_url' => 'https://facebook.com',
                                       'linkedin_url' => 'https://linkedin.com']);
         $response->assertStatus(200);
    }

    public function testPutProfile()
    {
         $response = $this->put('/api/users/1/profile',
                                 ['ddn'=> '2016-12-15',
                                  'web_site_url'=>'http://google.ca/test',
                                  'facebook_url'=>'https://facebook.com/test',
                                  'linkedin_url'=>'https://linkedin.com/test'],
                                 ['Accept'=> 'application/json']);

        $response->assertJsonFragment(['ddn'=> '2016-12-15',
                                       'web_site_url'=>'http://google.ca/test',
                                       'facebook_url'=>'https://facebook.com/test',
                                       'linkedin_url'=>'https://linkedin.com/test']);
        $response->assertStatus(200);
    }

    public function testPutProfileWithoutddn()
    {
         $response = $this->put('/api/users/1/profile',
                                 ['web_site_url'=>'http://google.ca/test',
                                  'facebook_url'=>'https://facebook.com/test',
                                  'linkedin_url'=>'https://linkedin.com/test'],
                                 ['Accept'=> 'application/json']);

        $response->assertJsonFragment(['ddn'=>["The ddn field is required."]]);
        $response->assertStatus(422);
    }

    public function testPutProfileWithoutWebSiteURL()
    {
         $response = $this->put('/api/users/1/profile',
                                 ['ddn'=> '2016-12-15',
                                  'facebook_url'=>'https://facebook.com/test',
                                  'linkedin_url'=>'https://linkedin.com/test'],
                                 ['Accept'=> 'application/json']);

        $response->assertJsonFragment(['web_site_url'=>["The web site url field is required."]]);
        $response->assertStatus(422);
    }

    public function testPutProfileWithoutFacebookURL()
    {
         $response = $this->put('/api/users/1/profile',
                                 ['ddn'=> '2016-12-15',
                                  'web_site_url'=>'http://google.ca/test',
                                  'linkedin_url'=>'https://linkedin.com/test'],
                                 ['Accept'=> 'application/json']);

        $response->assertJsonFragment(['facebook_url'=>["The facebook url field is required."]]);
        $response->assertStatus(422);
    }

    public function testPutProfileWithoutLinkedInURL()
    {
         $response = $this->put('/api/users/1/profile',
                                 ['ddn'=> '2016-12-15',
                                  'web_site_url'=>'http://google.ca/test',
                                  'facebook_url'=>'https://facebook.com/test'],
                                 ['Accept'=> 'application/json']);

        $response->assertJsonFragment(['linkedin_url'=>["The linkedin url field is required."]]);
        $response->assertStatus(422);
    }

    public function testPutProfileWithInvalidDDN()
    {
         $response = $this->put('/api/users/1/profile',
                                 ['ddn'=> '2016-12-155',
                                  'web_site_url'=>'http://google.ca/test',
                                  'facebook_url'=>'https://facebook.com/test',
                                  'linkedin_url'=>'https://linkedin.com/test'],
                                 ['Accept'=> 'application/json']);

        $response->assertJsonFragment(['ddn'=>["The ddn is not a valid date."]]);
        $response->assertStatus(422);
    }
}
