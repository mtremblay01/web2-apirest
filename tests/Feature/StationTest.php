<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StationTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */

     //Post
    public function testPostStation()
    {
         $response = $this->post('/api/stations',
                                  ['description'=> 'Colombourg', 'lat'=>49.03, 'long' => -78.06],
                                  ['Accept'=> 'application/json']);
         $response->assertJsonFragment(['description'=> 'Colombourg']);
         $response->assertStatus(200);
    }

    public function testPostStationWithoutDesciption()
    {
         $response = $this->post('/api/stations',
                                  ['lat'=>'49.03', 'long' => -78.06],
                                  ['Accept'=> 'application/json']);
         $response->assertJsonFragment(['The description field is required.']);
         $response->assertStatus(422);
    }

    public function testPostStationWithoutLat()
    {
         $response = $this->post('/api/stations',
                                  ['description'=> 'Colombourg', 'long' => -78.06],
                                  ['Accept'=> 'application/json']);
         $response->assertJsonFragment(['The lat field is required.']);
         $response->assertStatus(422);
    }

    public function testPostStationWithoutLong()
    {
         $response = $this->post('/api/stations',
                                  ['description'=> 'Colombourg', 'lat'=>49.03],
                                  ['Accept'=> 'application/json']);
         $response->assertJsonFragment(['The long field is required.']);
         $response->assertStatus(422);
    }

    public function testPostStationWithInvalidLat()
    {
         $response = $this->post('/api/stations',
                                  ['description'=> 'Colombourg', 'lat'=>'notANumber', 'long' => -78.06],
                                  ['Accept'=> 'application/json']);
         $response->assertJsonFragment(['The lat must be a number.']);
         $response->assertStatus(422);
    }

    public function testPostStationWithInvalidLong()
    {
         $response = $this->post('/api/stations',
                                  ['description'=> 'Colombourg', 'lat'=>48, 'long' => 'notANumber'],
                                  ['Accept'=> 'application/json']);
         $response->assertJsonFragment(['The long must be a number.']);
         $response->assertStatus(422);
    }

    //Get
    public function testGetStation(){
         $response = $this->get('/api/stations');

         $response->assertJsonFragment(['description' => 'Macamic',
                                        'lat' => 48.76,
                                        'long' => -79.06,
                                        'description' => 'La Sarre',
                                        'lat' => 49.03,
                                        'long' => -79.56]);
         $response->assertStatus(200);
    }

    public function testGetStation1(){
         $response = $this->get('/api/stations/1');

         $response->assertJsonFragment(['description' => 'Macamic',
                                       'lat' => 48.76,
                                       'long' => -79.06]);
         $response->assertStatus(200);
    }

    public function testGetStation2(){
         $response = $this->get('/api/stations/2');

         $response->assertJsonFragment([ 'description' => 'La Sarre',
                                        'lat' => 49.03,
                                        'long' => -79.56]);
         $response->assertStatus(200);
    }

    public function testGetUnexistingStation(){
         $response = $this->get('/api/stations/3');
         
         $response->assertStatus(404);
    }

    //Put
    public function testPutProfile()
    {
         $response = $this->put('/api/stations/1',
                                 ['description'=> 'Colombourg',
                                  'lat'=>'45.96',
                                  'long'=>'-78.21'],
                                 ['Accept'=> 'application/json']);

        $response->assertJsonFragment(['description'=> 'Colombourg',
                                       'lat'=>'45.96',
                                       'long'=>'-78.21']);
        $response->assertStatus(200);
    }

    public function testPutProfileWithoutDescription()
    {
         $response = $this->put('/api/stations/1',
                                 ['lat'=>'45.96',
                                  'long'=>'-78.21'],
                                 ['Accept'=> 'application/json']);

        $response->assertJsonFragment(['The description field is required.']);
        $response->assertStatus(422);
    }

    public function testPutProfileWithoutLat()
    {
         $response = $this->put('/api/stations/1',
                                 ['description'=> 'Colombourg',
                                  'long'=>'-78.21'],
                                 ['Accept'=> 'application/json']);

        $response->assertJsonFragment(['The lat field is required.']);
        $response->assertStatus(422);
    }

    public function testPutProfileWithoutLong()
    {
         $response = $this->put('/api/stations/1',
                                 ['description'=> 'Colombourg',
                                  'lat'=>'45.96'],
                                 ['Accept'=> 'application/json']);

        $response->assertJsonFragment(['The long field is required.']);
        $response->assertStatus(422);
    }

    public function testPutProfileWithInvalidLat()
    {
         $response = $this->put('/api/stations/1',
                                 ['description'=> 'Colombourg',
                                  'lat'=>'notANumber',
                                  'long'=>'-78.21'],
                                 ['Accept'=> 'application/json']);

        $response->assertJsonFragment(['The lat must be a number.']);
        $response->assertStatus(422);
    }

    public function testPutProfileWithInvalidLong()
    {
         $response = $this->put('/api/stations/1',
                                 ['description'=> 'Colombourg',
                                  'lat'=>'45.96',
                                  'long'=>'notANumber'],
                                 ['Accept'=> 'application/json']);

        $response->assertJsonFragment(['The long must be a number.']);
        $response->assertStatus(422);
    }

    //Delete
    public function testDeleteStation(){
        $deleteResponse = $this->delete('/api/stations/1');
        $getResponse = $this->get('/api/stations/1');

        $deleteResponse->assertStatus(200);
        $getResponse->assertStatus(404);
    }
}
