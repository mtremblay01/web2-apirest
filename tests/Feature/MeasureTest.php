<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MeasureTest extends TestCase
{
     use DatabaseTransactions;

     /************************************************
     Test of Post Method
     ************************************************/
     public function testPostMeasure()
     {
          $response = $this->post('/api/measures',
                                   ['station_id' => 1,
                                    'value'=> '3', 'description'=>'co2'],
                                   ['Accept'=> 'application/json']);
          $response->assertJsonFragment(['value'=>'3']);
          $response->assertStatus(200);
     }

     public function testPostMeasureWithoutValueTest()
     {
          $response = $this->post('/api/measures',
                                   ['station_id' => 1,'description'=>'co2'],
                                   ['Accept'=> 'application/json']);
          $response->assertJsonFragment(["value"=>["The value field is required."]]);
          $response->assertStatus(422);
     }

     public function testPostMeasureWithoutDescriptionTest()
     {
          $response = $this->post('/api/measures',
                                   ['station_id' => 1,'value'=>'2'],
                                   ['Accept'=> 'application/json']);
          $response->assertJsonFragment(["description"=>["The description field is required."]]);
          $response->assertStatus(422);
     }

     public function testPostMeasureWithoutNumericValueTest()
     {
          $response = $this->post('/api/measures',
                                   ['station_id' => 1,'value' => 'three', 'description'=>'co2'],
                                   ['Accept'=> 'application/json']);
          $response->assertJsonFragment(["value"=>["The value must be a number."]]);
          $response->assertStatus(422);
     }

     public function testPostMeasureWithInavlidDescrition()
     {
          $response = $this->post('/api/measures',
                                   ['station_id' => 1,'value' => 'three', 'description'=> 2],
                                   ['Accept'=> 'application/json']);
          $response->assertJsonFragment(["description"=>["The description must be a string."]]);
          $response->assertStatus(422);
     }

     /************************************************
     Test of Get Method
     ************************************************/

     public function testGetMeasure(){
          $response = $this->get('/api/measures');
          $response->assertJsonFragment(
                                   ['value' => 15, 'description' => 'CO2'],
                                   ['value' => 25, 'description' => 'CO2'],
                                   ['value' => 10, 'description' => 'PH10'],
                                   ['value' => 13, 'description' => 'PH10'],
                                   ['Accept'=> 'application/json']
                              );
          $response->assertStatus(200);
     }

     public function testGetMeasureOne(){
          $response = $this->get('/api/measures/1',
                                   ['Accept'=> 'application/json']);

          $response->assertJsonFragment(['value' => 15, 'description' => 'CO2']);
          $response->assertStatus(200);
     }

     public function testGetMeasureTwo(){
          $response = $this->get('/api/measures/2',
                                   ['Accept'=> 'application/json']);

          $response->assertJsonFragment(['value' => 25, 'description' => 'CO2']);
          $response->assertStatus(200);
     }

     public function testGetMeasureThree(){
          $response = $this->get('/api/measures/3',
                                   ['Accept'=> 'application/json']);

          $response->assertJsonFragment(['value' => 10, 'description' => 'PH10']);
          $response->assertStatus(200);
     }

     public function testGetMeasureFour(){
          $response = $this->get('/api/measures/4',
                                   ['Accept'=> 'application/json']);

          $response->assertJsonFragment(['value' => 13, 'description' => 'PH10']);
          $response->assertStatus(200);
     }

     /************************************************
     Test of Put Method
     ************************************************/

     public function testPutMeasureOne(){
          $response = $this->put('/api/measures/1',
                                   ['station_id' => 1,'value' => 25, 'description' => 'PH10'],
                                   ['Accept'=> 'application/json']);

          $response->assertJsonFragment(['value' => 25, 'description' => 'PH10']);
          $response->assertStatus(200);
     }

     public function testPutMeasureOneWithInvalidValue(){
          $response = $this->put('/api/measures/1',
                                   ['station_id' => 1,'value' => 'aString', 'description' => 'PH10'],
                                   ['Accept'=> 'application/json']);

          $response->assertJsonFragment(["value"=>["The value must be a number."]]);
          $response->assertStatus(422);
     }

     public function testPutMeasureOneWithInvalidDescription(){
          $response = $this->put('/api/measures/1',
                                   ['station_id' => 1,'value' => 25, 'description' => 422],
                                   ['Accept'=> 'application/json']);

          $response->assertJsonFragment(["description"=>["The description must be a string."]]);
          $response->assertStatus(422);
     }

     public function testPutMeasureOneWithoutValue(){
          $response = $this->put('/api/measures/1',
                                   ['station_id' => 1,'description' => 'PH10'],
                                   ['Accept'=> 'application/json']);

          $response->assertJsonFragment(["value"=>["The value field is required."]]);
          $response->assertStatus(422);
     }

     public function testPutMeasureOneWithoutDescription(){
          $response = $this->put('/api/measures/1',
                                   ['station_id' => 1,'value' => 25],
                                   ['Accept'=> 'application/json']);

          $response->assertJsonFragment(["description"=>["The description field is required."]]);
          $response->assertStatus(422);
     }

     /************************************************
     Test of Destroy Method
     ************************************************/

     public function testDestroyOnMeasureOne(){
          $deleteResponse = $this->delete('/api/measures/1');

          $getResponse = $this->get('/api/measures/1',
                                   ['Accept'=> 'application/json']);
          $deleteResponse->assertStatus(200);
          $getResponse->assertStatus(404);
     }
}
